'use strict'

const Parser = require('../lib/ltx')

function strLengthInBytes (str) {
  let textLength = 0
  for (let i = 0; i < str.length; i++) {
    textLength += Buffer.byteLength(str[i])
  }
  return textLength
}

const streamRanges = []

describe('SAX parsing', () => {
  let parser

  beforeEach(() => {
    parser = new Parser()
  })

  it('should emit startElement with tag name', (done) => {
    const tag = 'element'
    parser.on('startElement', (tagname) => {
      expect(tagname).toEqual(tag)
      done()
    })

    parser.write(`<${tag}>`)
  })

  it('should emit startElement with attributes', (done) => {
    const attributeValue = 'an attribute'

    parser.on('startElement', (tagname, attrs) => {
      expect(attrs).toEqual({'attribute': `${attributeValue}`})
      done()
    })

    parser.write(`<element attribute="${attributeValue}">`)
  })

  it('should emit endElement with tag name', (done) => {
    const tag = 'element'
    parser.on('endElement', (tagname) => {
      expect(tagname).toEqual(tag)
      done()
    })

    parser.write(`</${tag}>`)
  })

  it(
    'should emit startElement and endElement if the tag is self closing',
    (done) => {
      let eventCounter = 0

      function isDone () {
        if (eventCounter === 2) {
          done()
        }
      }

      parser.on('startElement', () => {
        eventCounter++
        isDone()
      })

      parser.on('endElement', () => {
        eventCounter++
        isDone()
      })

      parser.write('<element/>')
    }
  )

  it(
    'should emit startElement with elementContentStartByte',
    (done) => {
      parser.on('startElement', (_, __, startAtByte) => {
        expect(startAtByte).toEqual(0)

        done()
      })

      parser.write('<element>')
    }
  )

  it(
    'should emit startElement with startAtByte',
    (done) => {
      parser.on('endElement', (_, startAtByte) => {
        expect(startAtByte).toEqual(xmlText.length)

        done()
      })

      const xmlText = '<element>text'

      parser.write(`${xmlText}</element>`)
    }
  )

  it('should emit endText with a streamRange', (done) => {
    let eventCounter = 0

    const firstCellText = 'Cell Text'
    const secondCellText = 'More Text'

    parser.on('endText', (streamRange) => {
      eventCounter++
      streamRanges.push(streamRange)
      if (eventCounter === 2) {
        expect(streamRanges).toEqual([{
          startAtByte: strLengthInBytes('<c>'),
          length: strLengthInBytes(firstCellText)
        }, {
          startAtByte: strLengthInBytes(
            `<c>${firstCellText}</c><do-not-emit></do-not-emit><c>`
          ),
          length: strLengthInBytes(secondCellText)
        }])
        done()
      }
    })

    parser.write(
      `<c>${firstCellText}</d><do-not-emit></do-not-emit><c>${secondCellText}</c>`
    )
  })

  it('should emit unescaped partial text inside text nodes', (done) => {
    const unescape = require('../lib/escape').unescapeXML
    const cellText = `normal text &not-real &a &amp; &#38&#38; &lt; &#60;
    &gt;&#62;&quot;&#34;&apos;&#39; &f;`
    let accText = ''

    parser
      .on('partialText', (partialText) => {
        accText += partialText
      })
      .on('endText', () => {
        expect(accText).toEqual(unescape(cellText))

        done()
      })

    parser.write(`<c>${cellText}</c>`)
  })

  it('should emit unescaped partial text inside text nodes', (done) => {
    const cellText = `${'four'.repeat(129)}!`
    const expectedPartialTexts = Math.ceil(cellText.length / 129)
    let actualPartialTexts = 0

    parser
      .on('partialText', () => {
        actualPartialTexts++
      })
      .on('endText', () => {
        expect(actualPartialTexts).toEqual(expectedPartialTexts)
        done()
      })

    parser.write(`<c>${cellText}</c>`)
  })
})
