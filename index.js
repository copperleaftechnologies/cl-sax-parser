(() => {
  'use strict'

  var escape = require('./lib/escape')
  var parser = require('./lib/ltx.js')

  parser.prototype.escapeXML = escape.escapeXML
  parser.prototype.unescapeXML = escape.unescapeXML
  parser.prototype.escapeXMLText = escape.escapeXMLText
  parser.prototype.unescapeXMLText = escape.unescapeXMLText

  module.exports = parser
})()
