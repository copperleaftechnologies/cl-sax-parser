'use strict'

const inherits = require('inherits')
const EventEmitter = require('events').EventEmitter
const unescapeXML = require('./escape').unescapeXML

const STATE_TEXT = 0
const STATE_IGNORE_TAG = 1
const STATE_TAG_NAME = 2
const STATE_TAG = 3
const STATE_ATTR_NAME = 4
const STATE_ATTR_EQ = 5
const STATE_ATTR_QUOT = 6
const STATE_ATTR_VALUE = 7

const SaxLtx = module.exports = function SaxLtx () {
  EventEmitter.call(this)

  let state = STATE_TEXT
  let escapedText
  let partialText = ''
  let remainder
  let tagName
  let attrs
  let endTag
  let selfClosing
  let attrQuote
  let recordStart = 0
  let recordStartInBytes = 0
  let attrName

  /**
   * Pointer to the current position in terms of bytes since the parser
   * has been created
   */
  let posInBytes = 0
  let elementStartAtByte

  /* Records how many bytes have been written inside a text node */
  let recordByteLength = 0

  this._handleTagOpening = function (endTag, tagName, attrs) {
    if (!endTag) {
      this.emit('startElement', tagName, attrs, elementStartAtByte)
      elementStartAtByte = undefined
      if (selfClosing) {
        this.emit('endElement', tagName, elementStartAtByte)
      }
    } else {
      this.emit('endElement', tagName, elementStartAtByte)
    }
  }

  this.write = function (data) {
    if (typeof data !== 'string') {
      data = data.toString()
    }
    let pos = 0

    /* Anything from previous write()? */
    if (remainder) {
      data = remainder + data
      pos += remainder.length
      remainder = null
    }

    function endRecording () {
      if (typeof recordStart === 'number') {
        const recorded = data.slice(recordStart, pos)
        recordStart = undefined
        return recorded
      }
    }

    function endTextRecording () {
      if (typeof recordStart === 'number') {
        const streamRange = Object.freeze({
          startAtByte: recordStartInBytes,
          length: recordByteLength
        })

        recordByteLength = 0
        recordStartInBytes = undefined
        return streamRange
      }
    }

    /**
     * Accumulates the character passed. Unless it is part of an escape sequence,
     * then it will store characters until the escape sequence is complete and
     * then accumulate the unescaped character(s).
     *
     * @param c - character to emits
     */
    function accumulatePartialText (c) {
      if (!escapedText && c === 60 /* < */) return

      if (!escapedText && c === 38 /* & */) {
        escapedText = '&'
      } else if (escapedText) {
        if (c === 38) { // start of a new escape sequence before the last one ended
          partialText += escapedText

          escapedText = '&'
        } else if (c !== 60) {
          escapedText += String.fromCharCode(c)
        }

        if (
          c === 59 || // ;
          c === 60 || // >
          escapedText.length > 6 // 6 characters is longest an xml escape sequence can be
        ) {
          partialText += unescapeXML(escapedText)

          escapedText = undefined
        }
      } else {
        partialText += String.fromCharCode(c)
      }
    }

    for (; pos < data.length; pos++) {
      const c = data.charCodeAt(pos)
      const charSize = Buffer.byteLength(data[pos])

      switch (state) {
        case STATE_TEXT:
          accumulatePartialText.call(this, c)

          if (partialText.length >= 129) {
            this.emit('partialText', partialText)
            partialText = ''
          }

          if (c === 60 /* < */) {
            if (partialText !== '') {
              this.emit('partialText', partialText)
              partialText = ''
            }

            const streamRange = endTextRecording()

            if (streamRange && streamRange.length !== 0) {
              this.emit('endText', streamRange)
            }
            state = STATE_TAG_NAME

            attrs = {}
            elementStartAtByte = posInBytes
            recordStart = pos + 1
          } else {
            recordByteLength += charSize
          }
          break
        case STATE_TAG_NAME:
          if (c === 47 /* / */ && recordStart === pos) {
            recordStart = pos + 1
            endTag = true
          } else if (c === 33 /* ! */ || c === 63 /* ? */) {
            recordStart = undefined
            state = STATE_IGNORE_TAG
          } else if (c <= 32 || c === 47 /* / */ || c === 62 /* > */) {
            tagName = endRecording()
            pos--
            posInBytes -= charSize
            state = STATE_TAG
          }
          break
        case STATE_IGNORE_TAG:
          if (c === 62 /* > */) {
            state = STATE_TEXT
          }
          break
        case STATE_TAG:
          if (c === 62 /* > */) {
            this._handleTagOpening(endTag, tagName, attrs)
            tagName = undefined
            attrs = undefined
            endTag = undefined
            selfClosing = undefined
            state = STATE_TEXT
            recordStart = pos + 1
            recordStartInBytes = posInBytes + 1
          } else if (c === 47 /* / */) {
            selfClosing = true
          } else if (c > 32) {
            recordStart = pos
            state = STATE_ATTR_NAME
          }
          break
        case STATE_ATTR_NAME:
          if (c <= 32 || c === 61 /* = */) {
            attrName = endRecording()
            pos--
            posInBytes -= charSize
            state = STATE_ATTR_EQ
          }
          break
        case STATE_ATTR_EQ:
          if (c === 61 /* = */) {
            state = STATE_ATTR_QUOT
          }
          break
        case STATE_ATTR_QUOT:
          if (c === 34 /* " */ || c === 39 /* ' */) {
            attrQuote = c
            state = STATE_ATTR_VALUE
            recordStart = pos + 1
          }
          break
        case STATE_ATTR_VALUE:
          if (c === attrQuote) {
            attrs[attrName] = unescapeXML(endRecording())
            attrName = undefined
            state = STATE_TAG
          }
          break
      }
      posInBytes += charSize
    }

    if (typeof recordStart === 'number' &&
      recordStart <= data.length) {
      remainder = data.slice(recordStart)
      recordStart = 0
    }
  }
}
inherits(SaxLtx, EventEmitter)

SaxLtx.prototype.end = function (data) {
  if (data) {
    this.write(data)
  }

  this.write = function () {
  }
}
